<?php

include("base.php");

include("database.php");

class Dll_api extends Base {

	private $response;
	
	private $conn;

	public function __construct($db) {
		$this->conn = $db;
		$this->response = array();
		$action = $this->request('action');
		$this->$action();
	}

	// Login Function  -  Parameter - username & password
	public function login(){
		$username = strtolower($_POST['username']);
		$password= md5($_POST['password']);
		
        $query = "SELECT * FROM tbl_users where username = '$username' AND password = '$password'";  

        $result = mysqli_query($this->conn, $query); 
		
		$data = mysqli_fetch_array($result,MYSQLI_ASSOC);
		
		if($data){
			$this->response = array('success' => true,
									'data' => json_encode($data),
									'message' => 'Login Successfully'
									);
		}else{
			$this->response = array('success' => false,
									'data' => '',
	    							'message' => 'invalid logins'
	    							);
		}
		$this->output($this->response);
	}
	
	// Register Function  -  username, password, name
	public function register(){
		$username = strtolower($_POST['username']);
		$password= md5($_POST['password']);
		$name = $_POST['name'];
		
		$result = mysqli_query($this->conn, "SELECT * FROM tbl_users where username= '$username'");
		$num_rows = mysqli_num_rows($result);
		if($num_rows >= 1){
			$this->response = array('success' => false,
									'data' => '',
	    							'message' => 'Username Already Exist'
	    							);
		}else{
			$query = "INSERT INTO tbl_users (username, password, name) VALUES ('$username','$password','$name')";
			$sql = mysqli_query($this->conn, $query);
			if($sql){
					$this->response = array('success' => true,
									'data' => '',
									'message' => 'Registration Successfully'
									);
			}
		}
		$this->output($this->response);
	}
	
	// Add User function  -  Parameter - username, password, name 
	public function add_user(){
		$username = strtolower($_POST['username']);
		$password = md5($_POST['password']);
		$name = $_POST['name'];
		
		$result = mysqli_query($this->conn, "SELECT * FROM tbl_users where username= '$username'");
		$num_rows = mysqli_num_rows($result);
		if($num_rows >= 1){
			$this->response = array('success' => false,
									'data' => '',
	    							'message' => 'Username Already Exist'
	    							);
		}else{
			$query = "INSERT INTO tbl_users (username, password, name) VALUES ('$username','$password','$name')";
			$sql = mysqli_query($this->conn, $query);
			if($sql){
					$this->response = array('success' => true,
									'data' => '',
									'message' => 'User Added Successfully'
									);
			}
		}
		$this->output($this->response);
	}
	
	// View User Function
	public function view_users(){
		$query = "SELECT * FROM tbl_users";  
        $result = mysqli_query($this->conn, $query); 
				
		while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {$rows[] = $row; }

		$this->response = array('success' => true,
									'data' => $rows,
									'message' => ''
									);
		$this->output($this->response);
	}
	
	// Add Seva  -  Parameter - name, description, lat, lon, date, time
	public function add_seva(){
		$name = $_POST['name'];
		$description = $_POST['description'];
		$lat = $_POST['lat'];
		$lon = $_POST['lon'];
		$date = $_POST['date'];
		$time = $_POST['time'];
		
		$query = "INSERT INTO tbl_seva (name, description, lat, lon, date, time) VALUES ('$name','$description','$lat','$lon','$date','$time')";
			$sql = mysqli_query($this->conn, $query);
			if($sql){
					$this->response = array('success' => true,
									'data' => '',
									'message' => 'Seva Added Successfully'
									);
			}else{
				$this->response = array('success' => false,
									'data' => '',
									'message' => 'Error while adding Seva'
									);
			}
		$this->output($this->response);
	}
	
	// List Seva 
	public function list_seva(){
		$query = "SELECT * FROM tbl_seva where date >= CURDATE() order by date";  
        $result = mysqli_query($this->conn, $query); 
				
		while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {$rows[] = $row; }

		$this->response = array('success' => true,
									'data' => $rows,
									'message' => ''
									);
		$this->output($this->response);
	}
	
	// Subscribe Seva  -  Parameter - user_id, seva_id, category_id
	public function subscribe_seva(){
		$user_id = $_POST['user_id'];
		$seva_id = $_POST['seva_id'];
		$cat_id	= $_POST['cat_id'];
		
		$result = mysqli_query($this->conn, "SELECT * FROM tbl_seva_register where seva_id= '$seva_id' AND user_id= '$user_id'");
		$num_rows = mysqli_num_rows($result);
		if($num_rows >= 1){
			$this->response = array('success' => false,
									'data' => '',
	    							'message' => 'You already subscribed to this seva'
	    							);
		}else{
			$query = "INSERT INTO tbl_seva_register (user_id, seva_id, cat_id) VALUES ('$user_id','$seva_id','$cat_id')";
			$sql = mysqli_query($this->conn, $query);
			if($sql){
					$this->response = array('success' => true,
									'data' => '',
									'message' => 'Subscribe to Seva is completed'
									);
			}else{
				$this->response = array('success' => true,
									'data' => '',
									'message' => 'Error while subscribing Seva'
									);
			}
		}

		$this->output($this->response);
	}
	
	// Get Seva Subscriber  -  Parameter - seva_id
	public function get_seva_subscriber(){
		$seva_id = $_POST['seva_id'];

		$query = "SELECT b.*, a.cat_id from tbl_seva_register as a LEFT JOIN tbl_users as b ON a.user_id = b.id WHERE seva_id = '$seva_id'";  
        $result = mysqli_query($this->conn, $query); 

		while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {$rows['subscribers'][] = $row; }
		
		
		$query_new = "SELECT id,name from tbl_category";
		$result_new = mysqli_query($this->conn, $query_new); 
		
		while($row_new = mysqli_fetch_array($result_new,MYSQLI_ASSOC)) {$rows_new[] = $row_new; }
		
		foreach($rows_new as $data){
			$array[$data['id']] = $data['name'];
		}

		$rows['category'] = $array;

		if(!empty($rows)){
			$this->response = array('success' => true,
									'data' => $rows,
									'message' => ''
									);
		}else{
			$this->response = array('success' => false,
									'data' => '',
									'message' => 'No Subscriber Yet'
									);
		}
		$this->output($this->response);
	}
	
	// Category Function
	public function category(){
		$query = "SELECT * from tbl_category";
		$result = mysqli_query($this->conn, $query); 
		
		while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {$rows[] = $row; }
		
		foreach($rows as $row){
			if($row['pid'] == 0){
				$array1[$row['id']] = $row['name']; 
			}else if($row['pid'] != 0){
				$array2[$row['pid']][] = $row['name']; 
			}
		}

		foreach($array1 as $key1=>$val1){
			foreach($array2 as $key2=>$val2){
				if($key1 == $key2){
					$finalarray[$val1] = $val2;
				}
			}
		}
		
		$this->response = array('success' => true,
									'data' => $finalarray,
									'message' => ''
									);
		$this->output($this->response);
	}

}


// get database connection
$database = new Database();
$db = $database->getConnection();

$obj = new Dll_api($db);
?>