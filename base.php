<?php
class Base {

	protected function request($key = '') {
		if($key) {
			return $_POST[$key];
		}
		return $_POST;
	}

	protected function pr($data = array()) {
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		die;
	}

	protected function output($data = array()) {
		if(!$data) {
			$data = ['isSuccess' => false];			
		}
		echo json_encode($data);
		exit;
	}


}