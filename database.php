<?php
class Database{
  
    // specify your own database credentials
    private $host = "localhost";
    private $db_name = "dll";
    private $username = "root";
    private $password = "delhilangar";
  
    // get the database connection
    public function getConnection(){

        $connection = null;

		// Create connection
		$connection = mysqli_connect($this->host, $this->username, $this->password, $this->db_name);
		// Check connection
		if ($connection->connect_error) {
			die("Connection failed: " . $connection->connect_error);
		} 

		return $connection; 

    }
}
?>